const dbConfig = require('../config/database');
const oracledb = require('oracledb');

module.exports.initialize = async function initialize(){
    const pool = await oracledb.createPool(dbConfig.somePool);
    let conn = await oracledb.getConnection();
}



module.exports.close = async function close(){
    await oracledb.getPool().close();
}

module.exports.checkForUnnotifiedMessages = async function checkForUnnotifiedMessages(){
    let conn = await oracledb.getConnection();
    //getting rows where is notified is 0, which means it is not notified
    let unnotifiedMessages = await conn.execute("SELECT * FROM Messages WHERE ISNOTIFIED=0");
    
    console.log(unnotifiedMessages, 'unnotifiedMessages');
    if(unnotifiedMessages.rows.length === 0){
        return false;
    }

    //collecting ids of rows to update ISNOTIFIED COLUMN
    let IDs = [];
    unnotifiedMessages.rows.map(c => {
        IDs.push(c[0]);
    });

    //set the isnotified field to 1 which means that it has been seen
    await conn.execute(`UPDATE MESSAGES SET ISNOTIFIED = 1 WHERE ID IN (${IDs.join(', ')})`,{},{
        autoCommit: true
    });

    return unnotifiedMessages.rows;
}

module.exports.simpleExecute = function simpleExecute(statement) {
    return new Promise(async (resolve, reject) => {
        let conn;
  
        try {
            conn = await oracledb.getConnection();
  
            const result = await conn.execute(statement, {}, {outFormat: oracledb.OBJECT});
            
            resolve(result);
        } 
        catch (err) {
            reject(err);
        } 
        finally {
            if (conn) { // conn assignment worked, need to close
                try {
                    await conn.close();
                } 
                catch (err) {
                console.log(err);
                }
            }
        }
    });
}

const database = require('./services/database');
const WebSocketServer = require('websocket').server;
const http = require('http');


async function startup(){
    console.log('Starting');
    
    //Setting server
    const server = http.createServer((requst, response) =>{

    });
    server.listen(3000, function(){
        console.log('Listening on port 3000');
    })

    //initializing db
    try{
        await database.initialize();
        console.log('Connected to db');
    }catch(err){
        console.error(err);
        process.exit(1);
    }

    wsServer = new WebSocketServer({
        httpServer: server
    });

    //user connects to the server socket
    wsServer.on('request', async function(request){
        console.log('Connection form origin' + request.origin + '.');
        
        var connection = request.accept(null, request.origin);

        try{
            var result = await database.simpleExecute('select * from Messages');
            console.log(result.rows);
        }catch(err){
            console.error(err);
            process.exit(1);
        }
        connection.sendUTF(
            JSON.stringify({type: 'hello-message', data: result.rows})
        );
        
        //constantly checks for unnotified messages
        //only iterates 9 times, I don't why, maybe setInterval issues.
        //maybe should use other type of iteration
        setInterval(() => {
            database.checkForUnnotifiedMessages()
                .then(messages => {
                    if(messages){
                        connection.sendUTF(
                            JSON.stringify({type: 'update-message', data: messages})
                        );
                    }
                });
        }, 3000);

        connection.on('init-message', function(message){
            console.log('Received message: '+ message.utf8Data);
            connection.sendUTF('Your message: ' + message.utf8Data);
        });
    });

}

startup();